/* 
 * File:   main.cpp
 * Author: heartless
 *
 * Created on 11 Июль 2014 г., 13:10
 */

#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

using namespace std;

char *itoa(long i, char* s) {
	sprintf(s, "%ld", i);
	return s;
}

int oct_to_dec(int octal){
	int decimal = 0,i = 0;
	while(octal!=0){
		 decimal = decimal + (octal % 10) * pow(8,i++);
		 octal = octal/10;
	}
	return decimal;
}

size_t strlcpy(char *dst, const char *src, size_t siz)
{
		char *d = dst;
		const char *s = src;
		size_t n = siz;

		/* Copy as many bytes as will fit */
		if (n != 0 && --n != 0) {
				do {
						if ((*d++ = *s++) == 0)
								break;
				} while (--n != 0);
		}

		/* Not enough room in dst, add NUL and traverse rest of src */
		if (n == 0) {
				if (siz != 0)
						*d = '\0';				/* NUL-terminate dst */
				while (*s++)
						;
		}

		return(s - src - 1);		/* count does not include NUL */
}


int main(int argc, char** argv) {
	char email[8];
	strlcpy(email, argv[1], 9);

	while(strlen(email) < 8){
		strcat(email,"A"); 
	}
	char *timestamp = new char[10];
	sprintf(timestamp, "%ld", (int) time(NULL));
	
	char *otp = new char[8];
	for(int i = 1; i < 9; i++){
		otp[i-1] = (((int)email[i-1])  % timestamp[i] % 8) + '0';
	}

	int otp_dec = oct_to_dec(atoi(otp));
	
	while(otp_dec > 999999){
		otp_dec -= 999999;
	}
	printf("%d", otp_dec);

	return 0;
}

