#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

using namespace std;

char *itoa(long i, char* s) {
	sprintf(s, "%ld", i);
	return s;
}

long oct_to_dec(long octal){
	long decimal = 0,i = 0;
	while(octal!=0){
		 decimal = decimal + (octal % 10) * pow(8,i++);
		 octal = octal/10;
	}
	return decimal;
}


size_t strlcpy(char *dst, const char *src, size_t siz)
{
		char *d = dst;
		const char *s = src;
		size_t n = siz;

		/* Copy as many bytes as will fit */
		if (n != 0 && --n != 0) {
				do {
						if ((*d++ = *s++) == 0)
								break;
				} while (--n != 0);
		}

		/* Not enough room in dst, add NUL and traverse rest of src */
		if (n == 0) {
				if (siz != 0)
						*d = '\0';				/* NUL-terminate dst */
				while (*s++)
						;
		}

		return(s - src - 1);		/* count does not include NUL */
}



int main(int argc, char** argv) {
	char *email = new char[8];
	strlcpy(email, argv[1], 9);

	char *otp = new char[6];
	strlcpy(otp, argv[2], 7);

	while(strlen(email) < 8){
		strcat(email,"A"); 
	}

	char *timestamp = new char[10];
	sprintf(timestamp, "%ld", (int) time(NULL));
	
	
	char *otp_oct = new char[9];
	sprintf(otp_oct, "%08o", atoi(otp));
	
	bool major_flag = false;


	while(strlen(otp_oct) < 9){
		major_flag = true;
		for(long i = 0; i < strlen(otp_oct); i++){
			long n_otp = otp_oct[i] - '0';
			if(n_otp != (email[i] % timestamp[i+1]) % 8){
				major_flag = false;
				break;
			}
		}
		if(major_flag == true)
			break;
		sprintf(otp, "%ld", (atoi(otp) + 999999));
		sprintf(otp_oct, "%08o", atoi(otp));
	}

	if(major_flag){
		printf("Success!\n");
		return 1;
	}
	else{
		printf("Fail!\n");
		return 0;
	}

	return 0;
}

