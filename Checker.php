<?
	include 'functions.php';

	class Checker {
		protected $ip;
		protected $port;
		protected $flag;
		protected $flag_id;

		protected $email;
		protected $app_state;
		protected $cookie;

		protected $payload_name;
		protected $prepath = '/volga/service_1/service';

		protected $potential_flags;

		public function __construct($endpoint, $flag_id, $flag){
			list($this->ip, $this->port) = explode(':', $endpoint);
			$this->flag_id = $flag_id;
			$this->flag = $flag;
			$this->email = sha1($flag_id).'@flager.com';
		}

		private function url($url){
			return 'http://'.$this->ip.':'.$this->port.$this->prepath.$url;
		}


		private function curlRequest($url, $params, &$rheaders, &$rbody){
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1847.137 Safari/537.36");
			curl_setopt($ch, CURLOPT_COOKIE, $this->cookie);
			if( $params != NULL){
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
			}
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
			curl_setopt($ch, CURLOPT_HEADER, true);
			
			$data = curl_exec($ch);
			
			$info = curl_getinfo($ch);
			$rbody = $info["size_download"] ? substr($data, $info["header_size"], $info["size_download"]) : "";
		
			$rheaders = substr($data, 0, $info["header_size"]);
			curl_close ($ch);

			return $info['http_code'];
		}

		private function setCookie($headers){
			$arr = explode("\n",$headers);
			foreach ($arr as $line) {
				if(preg_match('#PHPSESSID=\w*;#',  $line, $matches)){
					$this->cookie = $matches[0];
					return TRUE;
				}
			}
			return FALSE;
		}

		private function setAppState($body){
			preg_match('#Welcome to (.*?)-files warehouse!#si', $body, $matches);

			$app = array(
							'bmp' => array('type' => 'image/bmp', 'ext' => 'bmp'),
							'gif' => array('type' => 'image/gif', 'ext' => 'gif'),
							'jpg' => array('type' => 'image/jpeg', 'ext' => 'jpg'),
							'png' => array('type' => 'image/png', 'ext' => 'png'),
							'XML' => array('type' => 'text/xml', 'ext' => 'xml'),
							'zip' => array('type' => 'application/zip', 'ext' => 'zip'),
							'sqlite' => array('type' => 'application/octet-stream', 'ext' => 'sqlite'),
							'Excel' => array('type' => 'application/vnd.ms-excel', 'ext' => 'xls'),
						);

			if(!in_array($matches[1], array_keys($app)))
				return FALSE;
			$this->app_state = $app[$matches[1]]['ext'];
			$this->app_type =  $app[$matches[1]]['type'];

			return TRUE;
		}

		public function isUp(){
			$code = $this->curlRequest( $this->url('/'), 0, $headers, $body );
			return $code == 404 ? FALSE : TRUE;
		}

		private function register(){
			$params = "email=".urlencode($this->email);
			$this->curlRequest( $this->url('/RegisterController/regRegister/'), $params, $headers, $body );

			if(preg_match('#Registered successfuly#si', $body))
				return TRUE;
			return FALSE;
		}

		private function login(){
			exec("./gen ".$this->email, $out);
			$otp = $out[0];
			$params = "email=".urlencode($this->email).'&otp='.urlencode($otp);
			$this->curlRequest( $this->url('/UserController/userLogin/'), $params, $headers, $body );
			
			return ($this->setCookie($headers) && $this->setAppState($body) && preg_match('#Files Page#si', $body)); 
		}


		private function uploadFlag(){
			$params = array('file'=>'@'.__DIR__.'/'.$this->payload_name.";type={$this->app_type};");
			$this->curlRequest( $this->url('/FileController/fileUpload/'), $params, $headers, $body );
			unlink(__DIR__.'/'.$this->payload_name);
			return !preg_match('#Validation failed#', $body);

		}

		private function downloadFlag(){
			$this->curlRequest( $this->url('/'), 0, $headers, $body );
			preg_match_all('#<a href=["\']?[^\'">]*?["\']?>#si', $body, $matches);
			
			$download = [];
			foreach ($matches[0] as $key => $value) {
				if(preg_match('#fileDownload#si', $value)){
					preg_match('#href=[\'"]?([^\'"]*)[\'"]?#sim', $value, $name);
					$download[] = @array_pop(explode('/', $name[1])); 
				}				
			}
			foreach ($download as $link) {
				$this->curlRequest( $this->url('/FileController/fileDownload/'.$link), 0, $headers, $body );
				file_put_contents($link, $body);
			}

			if(empty($download))
				return FALSE;
			$this->potential_flags = $download;
			
			return TRUE;

		}

		private function generateFlagFile(){
			
			$this->payload_name = md5($this->flag).'.'.$this->app_state;
			$this->app_state;

			switch ($this->app_state) {
				case 'bmp':
				case 'png':
				case 'gif':
				case 'jpg':
					$im = imagecreatetruecolor(1000, 100);

					$white = imagecolorallocate($im, 255, 255, 255);
					$black = imagecolorallocate($im, 0, 0, 0);
					imagefilledrectangle($im, 0, 0, 999, 99, $white);

					$text = $this->flag;
					$font = './arial.ttf';
					
					imagettftext($im, 38, 0, 20, 40, $black, $font, $text);
					
					$function = "image".($this->app_state == 'jpg' ? 'jpeg' : $this->app_state);
					$function($im, $this->payload_name);
					imagedestroy($im);
					break;
				
				case 'xml':
					$xroot = new SimpleXMLElement("<root></root>");
					$xroot->addAttribute('Truth', 'is out there');
					$xflag = $xroot->addChild('flag', $this->flag);
					$xroot->asXML($this->payload_name);
					break;
				case 'zip':
					$zip = new ZipArchive();
					
					$zip->open($this->payload_name, ZipArchive::CREATE);
					$zip->addFromString("flag.txt", $this->flag);
					$zip->close();
					break;
				case 'sqlite':
					$db = new sqlite3($this->payload_name);
					$db->exec("CREATE TABLE if not exists flag (id INTEGER PRIMARY KEY autoincrement , flag CHAR(50))");
					$db->exec("INSERT INTO flag (flag) VALUES ('{$this->flag}')");
					$db->close();
					break;
				case 'xls':
					require_once './PHPExcel.php';

					require_once 'PHPExcel/Writer/Excel2007.php';

					$objPHPExcel = new PHPExcel();

					$objPHPExcel->setActiveSheetIndex(0);
					$objPHPExcel->getActiveSheet()->SetCellValue('A1', $this->flag);
					
					// Rename sheet
					$objPHPExcel->getActiveSheet()->setTitle('Flag sheet');

					$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
					$objWriter->save($this->payload_name);

					break;

				default:
					return FALSE;
					break;
			}
			return TRUE;
		}

		private function checkFlagFile(){


			foreach ($this->potential_flags as $name) {
				$ext = @array_pop(explode('.', $name));

				$this->payload_name = 'tmp'.mt_rand();
				

				switch ($ext) {
					case 'bmp':
					case 'png':
					case 'gif':
					case 'jpg':
						$im = imagecreatetruecolor(1000, 100);

						$white = imagecolorallocate($im, 255, 255, 255);
						$black = imagecolorallocate($im, 0, 0, 0);
						imagefilledrectangle($im, 0, 0, 999, 99, $white);

						$text = $this->flag;
						$font = './arial.ttf';
						
						imagettftext($im, 38, 0, 20, 40, $black, $font, $text);
						
						$function = "image".($this->app_state == 'jpg' ? 'jpeg' : $this->app_state);
						$function($im, $this->payload_name);
						imagedestroy($im);

						if(md5_file($name) == md5_file($this->payload_name)){
							@unlink($this->payload_name);
							return TRUE;
						}

						break;
					
					case 'xml':
						$xroot = new SimpleXMLElement("<root></root>");
						$xroot->addAttribute('Truth', 'is out there');
						$xflag = $xroot->addChild('flag', $this->flag);
						$xroot->asXML($this->payload_name);

						if(md5_file($name) == md5_file($this->payload_name)){
							@unlink($this->payload_name);
							return TRUE;
						}

						break;
					case 'zip':
						$zip = new ZipArchive();
						
						$zip->open($name);
						if($this->flag === $zip->getFromName('flag.txt')){
							@unlink($this->payload_name);
							return TRUE;
						}
						$zip->close();

						break;
					case 'sqlite':
						$db = new sqlite3($name);
						$db->exec("CREATE TABLE if not exists flag (id INTEGER PRIMARY KEY autoincrement , flag CHAR(50))");
						if($this->flag === $db->querySingle("SELECT flag from flag")){
							@unlink($this->payload_name);
							return TRUE;
						}
						$db->close();
						break;
					case 'xls':
						require_once './PHPExcel.php';

						require_once 'PHPExcel/Writer/Excel2007.php';

						$fileType = PHPExcel_IOFactory::identify($name);

						// Read the file
						$objReader = PHPExcel_IOFactory::createReader($fileType);
						$objPHPExcel = $objReader->load($name);

						if( $objPHPExcel->getActiveSheet()->getCell('A1')->getFormattedValue() == $this->flag){
							@unlink($this->payload_name);
							return TRUE;
						}
						
						unset($objPHPExcel);
						unset($objReader);

						
						break;

					default:
						break;
				}
				@unlink($this->payload_name);
			}
			return FALSE;
		}

		private function cleanUp(){
			foreach ($this->potential_flags as $name) {
					@unlink($name);
			}
		}

		public function push(){
			//isUp ?
			if( !$this->isUp())
				return 'DOWN';

			//register
			if( !$this->register())
				return 'MUMBLE_push_reg';

			//login using OTP
			if( !$this->login())
				return 'MUMBLE_push_login';

			//generate payload
			if( !$this->generateFlagFile())
				return 'MUMBLE_payload_gen';

			//upload file
			if( !$this->uploadFlag())
				return 'MUMBLE_push_upload';

			return 'OK';

		}

		public function pull(){
			//isUp ?
			if( !$this->isUp())
				return 'DOWN';

			//login using OTP
			if( !$this->login())
				return 'MUMBLE_login';

			//download file
			if( !$this->downloadFlag())
				return 'MUMBLE_download';

			//check flag
			if( !$this->checkFlagFile())
				return 'CORRUPTED';

			$this->cleanUp();

			return 'OK';
		}

	}
	`rm -f ../service/app/database/users.sqlite`;


	$checker = new Checker('192.168.1.175:80','1','0123456789abcdef0123456789abcdef1=');
	echo "push -> " . $checker->push();
	echo "<br>\n";
	$checker = new Checker('192.168.1.175:80','1','0123456789abcdef0123456789abcdef1=');
	echo "pull -> " .$checker->pull();