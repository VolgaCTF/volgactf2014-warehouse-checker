#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>


size_t strlcpy(char *dst, const char *src, size_t siz)
{
		char *d = dst;
		const char *s = src;
		size_t n = siz;

		/* Copy as many bytes as will fit */
		if (n != 0 && --n != 0) {
				do {
						if ((*d++ = *s++) == 0)
								break;
				} while (--n != 0);
		}

		/* Not enough room in dst, add NUL and traverse rest of src */
		if (n == 0) {
				if (siz != 0)
						*d = '\0';				/* NUL-terminate dst */
				while (*s++)
						;
		}

		return(s - src - 1);		/* count does not include NUL */
}



int main(int argc, char** argv) {
	srand(time(NULL));

	char period_c[5];
	strlcpy(period_c, argv[2], 9);
	int period = atoi(period_c);

	char dim_c[2];
	strlcpy(dim_c, argv[3], 3);
	int dim = atoi(dim_c);


	char *timestamp_c = new char[10];
	sprintf(timestamp_c, "%ld", (int) time(NULL));
	int timestamp = atoi(timestamp_c);

	struct stat sb;
	stat(argv[1], &sb);
	
	char * file_edited_c = new char[10];
	sprintf(file_edited_c,"%ld", (long long)sb.st_mtim.tv_sec);

	int file_edited = atoi(file_edited_c);

	if(timestamp - file_edited >= period){
		//printf("edit time: %ld ; now : %ld \nSetting new state!\n", file_edited, timestamp);
		int new_state = rand() % dim + 1;
		
		//printf("Dim %d : %d\n", dim, new_state);
		FILE *fp = fopen(argv[1], "w");
		if (fp == NULL) {
			printf("Error.\n");
			return 0;
		}
		else{
			fprintf(fp, "%d", new_state);
			fclose(fp);
		}
	}


	return 0;
}

